# reboot-notify

Small Python script which notifies on reboot of a host.

## Installation

Deploy the script to a location, e.g. */usr/bin/reboot-notify* and make it executable:

```
chmod +x /usr/bin/reboot-notify
```

Create the config file in */etc/reboot-notify.conf* and set values. An example can be found in *reboot-notify.conf*. It is a JSON dictionary. Also restrict permissions to the config file as it contains passwords:

```
chmod 400 /etc/reboot-notify.conf
```

Last create the systemd service by copying the file *reboot-notify.service* to */etc/systemd/system*:

```
cp reboot-notify.service /etc/systemd/system
systemctl daemon-reload
systemctl enable reboot-notify.service
```

### Pushsafer Push Notifications

If you want to use the Pushsafer push notification service, download the Pushsafer module from their Github page and copy it to the Python3 module directory, e.g. */usr/lib/python3/dist-packages/pushsafer.py* or use *pip* to install.

Also you may need to install the Python 3 package *requests*, e.g. for Debian systems:

```
apt-get install python3-requests
```

**NOTE** The Pushsafer module does not set a certificate store so there is a warning message displayed when run:

```
/usr/lib/python3/dist-packages/urllib3/connectionpool.py:845: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
  InsecureRequestWarning)
```

